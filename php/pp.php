<div class="part" id="pp">

	<div class="left section">
		<ul>
		  <li><h1>Projet pédagogique</h1></li>
			<li class="intro current">1.Introduction</li>
			<li class="contexte">1.Contexte</li>
			<li class="objectifs">2.Objectifs</li>
			<li class="methode">3.Méthode</li>
			<li class="deroulement">4.Déroulement</li>
			<li class="outillage">5.Outillage pédagogique</li>
		</ul>
	</div>

	<div class="right section">
		<h2 id="intro">Introduction</h2>
		<p>
			Ce projet émane d’un questionnement qui m’anime et alimente ma pratique - la place de la technique dans le processus de création - et découle d’un constat fait durant mes études complété par une démarche développée au sein de ma pratique professionnelle et personnelle.
		</p>
		<p>
			En effet, mon passage en tant qu’étudiant dans différentes écoles - Estienne et l’EnsAD à Paris,
l’Erg à Bruxelles – m’a permis de comparer et mettre en relation des méthodes pédagogiques différentes
et complémentaires. Durant ces études, sous l’œil avisé des enseignants, j’ai eu l’occasion de développer rigueur et expérimentation à travers les divers supports du design graphique, de l’édition au web en passant par la le dessin de caractère, la vidéo ou encore l’affiche. En parallèle, j’ai développé un intérêt pour les arts numérique et mis en place un travail d’expérimentation de dispositifs interactifs et génératifs, tant matériels que numériques.
		</p>
		<p>
			C’est sur ces bases que j’ai ensuite commencé à développer une approche de l’art et du design
par la technique, non pas comme une fin en soi mais comme un vecteur de créativité. Le point de départ
de cette réflexion peut se situer dans l’écriture de mon mémoire, Pour un design graphique libre, dans
lequel j’interrogeais les relations entre culture libre et design graphique. Dans le prolongement de cette idée,
mon projet de fin d’études, Interstices, questionnait les notions d’échange et de transmission par la mise
en place d’une boîte à outil favorisant la mise en commun des savoirs et des outils à travers le web, l’édition, l’installation ou encore le dessin de caractère. Au sortir de mes études, j’ai pu éprouver et approfondir cette démarche par une application professionnelle et personnelle, notamment en co-fondant l’Atelier Bek, espace de création et de diffusion situé à Molenbeek dont la volonté est d’établir un échange et une mutualisation des savoirs et savoirs-faire. Cette réflexion est maintenant prolongée avec Luuse, regroupement de cinq designers graphiques et développeurs.
		</p>
		<p>
			Ainsi, ce dossier pédagogique reflète d’une part ces désirs de transmission et de partage, et d’autre part la volonté de redéfinir la place de l’outil technique en l’intégrant pleinement dans une démarche créative. Ma volonté est de sensibiliser à un usage actif et réfléchi de l’outil dans le but de stimuler une attitude menant vers l’autonomie, l’expérimentation et le développement d’une culture technique.
		</p>
		<h2 id="contexte">Contexte</h2>
	  <p>
	    L'outil technique est au cœur de tout processus de création. En effet, chaque idée abstraite se matérialise inévitablement par l'utilisation d'une technique particulière, qui amènera une forme spécifique. L’inverse peut également se vérifier, la technique en tant que culture est une des constituantes de notre système
de pensée.  En partant de ce postulat, il s'agirait de s'écarter de l'opposition entre création et technique afin
de prendre en compte leurs complémentarités en considérant la technique comme un facteur de créativité
et non comme un mal nécessaire, une phase contraignante du processus créatif ou un simple service de mise en forme.
	  </p>
	  <p>
	  	Pour ce faire, il convient de choisir un outil adéquat et ouvert, par lequel la forme sera produite
avec le minimum de contraintes. Si celui-ci n'existe pas ou ne peut s'adapter à l'usage désiré, pourquoi
ne pourrions-nous pas modifier l'existant ou en concevoir de nouveaux?
	  </p>
	  <p>
	  	Le numérique est omniprésent dans nos vies. La génération actuelle d’étudiants est née avec
et l’utilise quotidiennement. Sans entrer dans l’opposition technophile contre technophobe, l’utilisation
des outils numériques est inévitable et probablement nécessaire. En revanche, il convient de développer
une attitude critique, que l’on pourrait qualifier de <i>technosceptique</i>, face à ces outils.
	  </p>
		<p>
			Plus précisément, le web est un outil difficilement négligeable dans le processus artistique. Qu’il soit simplement un outil de diffusion ou directement outil de création et sans forcément en devenir un expert, il semble important de maîtriser un minimum ses techniques et d’en comprendre les rouages culturels.
		</p>
		<img src="img/W3gW73i.jpg" alt="" />

	  <h2 id="objectifs">Objectifs</h2>
	  <p>
	    Mon implication dans ce cours se base sur deux objectifs, sensibiliser les étudiants sur la place
de la technique dans le processus créatif et les familiariser plus particulièrement aux techniques et à la culture de la programmation. Il en découle trois notions clefs sous-jacentes: développer le désir d'expérimentation de l'étudiant, accroître son autonomie et tendre vers son émancipation technique.
	  </p>
	  <p>
	  	La pratique de la programmation prend ici tout son sens. À l'inverse d'un logiciel dont la logique
se situe dans une série de choix (dé)limités par une interface graphique où le menu contextuel est de mise, l'usage du code, de par sa nature ouverte et aux potentiels théoriquement infinis, permet de rompre cette barrière qui soumet l'utilisateur à un panel de sélections déterminé par les créateurs du logiciel. Finalement, le logiciel est une interface graphique écrivant le code à notre place, pratiquer la programmation est donc
une manière de réduire les intermédiaires et d’ainsi élargir le champ des possibles. Elle peut dès lors devenir partie intégrante de la recherche créative, poussant son usager à développer une réflexion complète sur l'objet qu'il cherche à concrétiser, comme un prolongement direct de ses intentions. De cette manière, il a la possibilité de sortir d'une passivité technique pour en développer une pratique active, réfléchie et cohérente.
	  </p>
	  <p>
	  	De plus, la programmation étant un domaine qui implique un constant tâtonnement, son utilisateur peut apprendre à accepter l'erreur et l’inattendu, les considérer comme des impulsions créatives et inclure
ces notions comme partie intégrante de ses recherches. Il entretient ainsi un dialogue avec son outil, désormais source réflexive.
	  </p>
		<p>
			L'étudiant pourra ainsi adopter une posture où le bidouillage et le tâtonnement auront la possibilité de le mener à une ouverture technique. Partant d’un principe où moins nous nous cloisonnons et plus nous assimilons, plus il est aisé d'apprendre de nouvelles choses, il me semble d’avantage intéressant d'apprendre à apprendre et ainsi développer l'autonomie de l'étudiant. L'objectif est de tendre vers l'indépendance technique, de faire comprendre qu'elle peut être un facteur d'émancipation et pas simplement une contrainte à laquelle on doit se plier pour parvenir à ses fins.
		</p>
		<p>
			Tout ceci se vérifie et prend d’autant plus d’importance dans le domaine du web où il n’existe pas réellement de solution qualitative pour faire l’économie du code. Sans former des experts, l’objectif est de familiariser à ces outils et de sensibiliser à leurs potentiels pour leur permettre d’acquérir une autonomie suffisante dans leur pratique.
		</p>

		<img src="img/brucemau_tools-500x376.jpg" alt="" />

		<h2 id="methode">Méthode</h2>
	  <p>
	    La méthodologie adoptée pour tendre vers ces objectifs passe selon moi tant par les notions d'appropriation et de partage que par l'apprentissage concret et collectif.
	  </p>
	  <p>
	  	Il est important de sensibiliser l'étudiant aux potentiels offerts par l'appropriation technique.
Un simple usage de l'outil, même expert, peut rapidement devenir confortable et n’incite pas à la prise
de risque alors que celle-ci est une des conditions nécessaires à l’épanouissement de l’étudiant. Il s'agira donc d'abord de comprendre le fonctionnement de l'objet technique utilisé, de manière à se placer dans
un rapport d'invention avec celui-ci, de ne plus faire de différence entre concepteur et utilisateur, de remettre constamment en question son emploi et ses applications. Ainsi, l'étudiant sera à même d'adapter l'outil à
son usage plutôt que de s'adapter à lui. C'est également une manière de démystifier l’objet technique, d'ouvrir
la boîte noire pour mettre la main dans le cambouis et décomplexer sa complexité. La tendance est encore bien trop souvent à la phobie technique, supposément réservée à un cercle restreint d'érudits. Pourtant, écrire un code simple est à la portée de tous et si nous sommes capables d'utiliser un logiciel complexe comme Indesign, nous avons largement les capacités nécessaires pour comprendre la logique programmatique.
	  </p>
	  <p>
	    	Suivant ce raisonnement, il me paraît nécessaire de mettre en place une logique de documentation technique et artistique. Si nous nous sommes appropriés une technique, que nous avons pu en tirer un usage créatif et pertinent, pourquoi ne pas partager cette expérience, la rendre disponible pour que d'autres puissent faire de même et ainsi prolonger la démarche? J'aimerais donc développer avec les étudiants une méthodo-logie de travail basée sur la mise à disposition de leurs découvertes, leurs processus et leurs expériences.
Que les ressources, les méthodes et les références puissent servir à tous, permettant ainsi de comprendre
que les moyens ont autant d'importance que la fin. Ce sera l’occasion de positionner cette démarche dans le prolongement d’une culture existante et prolifique, la culture libre. La culture libre milite pour une libération des œuvres de l’esprit, encourageant leur libre circulation et modification. C’est pour moi une manière de sensibiliser les étudiants sur la valeur sociale de leur travail. Il s’agit de permettre à leur travail de continuer à vivre et évoluer au-delà de ce pourquoi il a été élaboré et ainsi enrichir un patrimoine intellectuel
dans lequel ils pourront eux-même puiser par la suite.
	  </p>
	  <p>
	  	Une telle méthodologie implique de se détacher autant que possible d'un apprentissage abstrait
et séparé d'applications concrètes. Il est essentiel de ne pas décourager et anéantir la soif de découverte
en donnant un cours unilatéral sur des notions inconnues et complexes sans se situer dans la mise en pratique directe d'une idée, sans en éprouver immédiatement l'intérêt. Il me paraît préférable d’associer autant que possible savoir et faire et permettre de mettre ses idées à l'épreuve de l'application pour stimuler le désir d'expérimentation de l'étudiant.
	  </p>
		<p>
			De la même manière, l'enseignant ne devrait pas détenir le monopole du savoir. Il me paraît important de développer une autonomie collective où chaque étudiant peut autant recevoir des autres
que leur donner. Cela passe par la favorisation du travail de groupe et la mise en place de dispositifs d'échanges collectifs tout en conservant de la place pour un suivi individuel et personnalisé. Une méthode envisageable serait la constitution de groupes de travail en fonction des centres d’intérêt de chacun. Cela permettrait de regrouper les objectifs des étudiants suivant les projets amenés et les désirs de chacun. Tout cela serait mené en encourageant une réflexion et une autonomie de groupe et où le rôle de l’enseignant
se situerait dans la recherche de cohésion et de stimulation par l’apport de concepts, de méthodes et
de références.
		</p>
		<p>
			Ce dernier point constitue selon moi une condition indispensable à un enseignement qui ne se réduit pas à un simple apprentissage technique. Le monde de la programmation existe autour d’une culture et d’une histoire qui lui sont propre et s’en imprégner permet d’en comprendre sa valeur sociale, artistique, économique et politique. C’est un stimulant dans l’apprentissage technique.
		</p>

		<img src="img/tumblr_mhe1os890Q1rdiyj4o1_1280.jpg" alt="" />

		<h2 id="deroulement">Déroulement</h2>
	  <p>
	    Je souhaite donc mettre en application les méthodes évoquées ci-dessus dans le déroulement de chaque cours. Ainsi, un cours type serait constitué de plusieurs « rubriques » s’imbriquant les unes aux autres :
	  </p>
	  <p>
			1 → Retour sur l’exercice donné au cours précédent.
			Un retour si possible détaillé et en groupe, basé tant sur les réussites que les échecs, de façon à approfondir des notions techniques et culturelles. De cette manière, l’apprentissage se fera à partir d’exemples concrets et chacun pourra apprendre du travail des autres.
	  </p>
		<p>
			2 → Tour d’horizon de l’actualité.
Afin d’alimenter cette partie, il sera demandé aux étudiants de procéder à une sélection de sujets d’actualité autour du web, du netArt, du web design, de nouveautés techniques ou plus généralement de questions de société. Leur analyse se fera collectivement et l’ensemble sera archivé afin de constituer une base de donnée commune.
		</p>
		<p>
			3 → Introduction d’une nouvelle notion technique.
Celle-ci se devra d’être dans le prolongement de ce qui a été fait précédemment, le moins abstraite possible et nourrie autant que possible d’un contexte théorique, historique et artistique.
		</p>
		<p>
			4 → Lancement d’un nouvel exercice
Cette exercice aura pour but de concrétiser la nouvelle notion technique abordée. Il ne devra pas être une simple mise en application mais devra comporter une valeur artistique. Si l’ampleur de celui-ci est trop important, il sera scindé sur plusieurs cours. Ainsi s’alterneront exercices courts et projets au plus long court ainsi que travail individuel et travail de groupe.
		</p>

		<h2 id="outillage">Cotation</h2>
	  <p>
	    L’évaluation se fera en continue. Les exercices et la sélection de sujets d’actualité seront la base de cette cotation. Celle-ci ne se fera pas uniquement sur la bonne réussite technique mais se fera aussi en partie sur la valeur artistique et la cohérence du rendu.
	  </p>
		<p>
			
		</p>
	</div>
</div>
