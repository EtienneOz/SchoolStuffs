<div class="part" id="plan">
	<div class="left section">
    <ul>
      <li><h1>hypothèses</h1></li>
      <li class="generalites">généralités</li>
      <li class="background">background</li>
      <li class="bases">les bases</li>
    </ul>
  </div>

  <div class="right section">

		<h2 id="generalites">généralités</h2>
		<p>
			Du concret, de la pratique. Le moins de concepts abstraits possibles. Des exercices pour apprendre. La correction pour comprendre. Donner envie, nourrir l'apprentissage d'exemples et de références. Faire comprendre l'intérêt de la programmation web.
		</p>
		<p>
			Deux approches: le webdesign et le netart.
		</p>
		<p>
			Un site individuel de documentation comme fil conducteur axé webdesign. Le site évoluera en fonction des compétences acquises.
		</p>
		<p>
			Notions à aborder:
			<ul>
				<li>
					Front-end:
					<ul>
						<li>html</li>
						<li>css</li>
						<li>css print</li>
						<li>préprocesseurs css (less, stylus)</li>
						<li>javascript</li>
						<li>librairies javascript (jquery, processingJs, node...)</li>
					</ul>
				</li>
				<li>
					Back-end:
					<ul>
						<li>php</li>
						<li>cms (Wordpress, Processwire...)</li>
						<li>cms flat-file</li>
						<li>méthodes alternatives</li>
						<li>bases de données (mysql, phpmyadmin, sqlite...)</li>
					</ul>
				</li>
				<li>serveurs:
					<ul>
						<li>ftp</li>
						<li>ssh</li>
					</ul>
				</li>
				<li>
					outils
					<ul>
						<li>git</li>
						<li>terminal</li>
					</ul>
				</li>
			</ul>
		</p>
		<h2 id="background">Background</h2>
		<p>
			<strike>
				Il paraît important en premier lieu de revenir de manière sommaire sur les notions environnant la programmation web:
				<ul>
					<li>Qu'est ce qu'internet?</li>
					<li>Qu'est ce que le web?</li>
					<li>Qu'est ce que la programmation?</li>
					<li>Qu'est ce qu'une url?</li>
					<li>Qu'est ce qu'un domaine?</li>
					<li>Qu'est ce qu'un serveur?</li>
					<li>Qu'est ce que le html?</li>
					<li>Qu'est ce que le css?</li>
					<li>Qu'est ce que le javascript?</li>
					<li>Qu'est ce que le php?</li>
					<li>...</li>
				</ul>
			</strike>
		</p>
		<p>
			Non, amener ces notions au compte-goutte pour de pas dégouter les étudiants. Eventuellement proposer une liste de ressources traitant du sujet.
		</p>

    <h2 id="bases">Les bases</h2>
    <p>
      Pour comprendre le fonctionnement de base du Html, le système de balisage et d'imbrication ainsi que la sémantique des balises, projeter en live une double fenêtre montrant d'un côté le html, de l'autre le résultat. Partir d'un texte brut et le baliser au fur-et-à-mesure des explications. Manière plus ludique de comprendre les fondamentaux qu'une explication abstraite.
    </p>
		<img src="img/html.gif" alt="" />
		<p>
			La même chose peut être envisagée pour le css.
		</p>
		<img src="img/css.gif" alt="" />
  </div>
</div>
