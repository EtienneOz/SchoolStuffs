<div class="part" id="notes">

	<div class="left section">
    <ul>
      <li><h1>Notes de réunions</h1></li>
      <li class="note1 current">01/05/17</li>
      <li class="note2">18/05/17</li>
    </ul>
  </div>

  <div class="right section">
    <h2 id="note1">01/05/17</h2>
    <ul>
      <li>Poste de conférencier en programmation web, 5h/semaine. Heures modulables</li>
      <li>
        Au sein de l'atelier arts numériques, 4 orientations théoriques en master:
        <ul>
          <li>interactivité</li>
          <li>electronique</li>
          <li>art immersif</li>
          <li>arts des réseaux</li>
        </ul>
      </li>
      <li>Depuis peu, les étudiants écrivent un mémoire en master</li>
      <li>L'apprentissage des outils se fait essentiellement en B1 et B2. Ensuite plutôt suivi de projet.</li>
      <li>Seulement 3 ou 4 étudiants en master, 30 sur l'ensemble de l'atelier</li>
      <li><a href="http://anwiki.arts2.be">anwiki.arts2.be</a> → wiki de documentation de l'atelier</li>
      <li>cours techniques / workshops / suivi</li>
      <li>2x2 semaines blanches sans cours mais workshops</li>
      <li>Il y a aussi des cours du soir <em>Creactif</em>, apprentissage Python + Processing, ouvert à tous. Désir de faire créditer</li>
      <li></li>
    </ul>
    <h2 id="note2">18/05/17</h2>
    <p>
      blabla
    </p>
  </div>

</div>
