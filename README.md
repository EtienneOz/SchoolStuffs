# SchoolStuffs

Site web regroupant recherches, hypothèses et ressources pédagogiques pour le cours de développement web d'Art² (Mons).

## captures

![image](/captures/1.png)
![image](/captures/2.png)
![image](/captures/3.png)

## live

[ici](http://www.etienneozeray.fr/schoolStuffs/)
