<?php if($debug == true) ini_set('display_errors', 'On'); ?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>SchoolStuffs</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css" />
    <link rel="stylesheet" type="text/css" href="style/main.css" />
    <!-- <link rel="stylesheet/less" type="text/css" href="style/main.less" /> -->
    <script type="text/javascript" src="js/lib/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/lib/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="js/lib/jquery.waypoints.min.js"></script>
    <script type="text/javascript" src="js/lib/less.min.js"></script>
  </head>
  <body>
