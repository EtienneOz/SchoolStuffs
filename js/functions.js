function showRessources(btn, el){
  btn.click(function(){
    el.slideDown();
  })
  el.find('.close').click(function(){
    el.slideUp();
  })
}

function scrollToPart(btn, el){
  btn.click(function(){
    var theClass = $(this).attr('class').split(' ')[0];
    if (theClass != 'ressources') {
      el.find('.left').children('ul').hide();
      var elToGo = el.children('#'+theClass)
      $(window).scrollTo(elToGo, 500);
      elToGo.find('.left').children('ul').show();
    }
  })
}

function changeMenuOnscroll(el, nav, content){
  el.each(function(){
    var theId = $(this).attr('id');
    var waypoint = new Waypoint({
      element: document.getElementById(theId),
      handler: function(direction){
        if (direction == 'down') {
          nav.children('.'+theId).addClass('current');
          nav.children('.'+theId).prev('li').removeClass('current');
          if (content) {
            content.children('#'+theId).prev().find('.left').children('ul').hide();
            content.children('#'+theId).find('.left').children('ul').show();
          }
        } else {
          nav.children('.'+theId).removeClass('current');
          nav.children('.'+theId).prev('li').addClass('current');
          if (content) {
            content.children('#'+theId).prev().find('.left').children('ul').show();
            content.children('#'+theId).find('.left').children('ul').hide();
          }
        }
      }
    })
  })
}
