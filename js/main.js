$(document).ready(function(){

  var ressources    = $('#ressources');
  var nav           = $('nav');
  var navBtn        = nav.children('ul').children('li');
  var btnRessources = nav.find('.ressources')
  var content       = $('#content')
  var part          = content.children('.part')
  var left          = content.find('.left').children('ul');
  var right         = content.find('.right');
  var leftBtn       = left.children('li');

  left.hide();
  left.eq(0).show();

  ressources.hide();

  showRessources(btnRessources, ressources);
  scrollToPart(navBtn, content);
  scrollToPart(leftBtn, right);


})

$(window).on('load', function(){

  var nav           = $('nav');
  var navList       = nav.children('ul');
  var content       = $('#content');
  var part          = content.children('.part');
  var right         = content.find('.right');
  var rightTitles   = right.children('h2');
  var left          = content.find('.left').children('ul');
  var leftBtn       = left.children('li');

  changeMenuOnscroll(part, navList, content);
  changeMenuOnscroll(rightTitles, left);
})
